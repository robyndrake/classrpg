class Game extends Phaser.Scene {
	constructor() {
		super({
			key: 'Game', 
			active: true
		});
	}

	preload() {
		this.load.image('button1', 'assets/images/ui/blue_button01.png');
	}

	create() {
		this.add.image(100, 100, 'button1');
	}
}

const config = {
	type: Phaser.AUTO,
	width: 800,
	height: 600,
	scene: [Game]
};

const master = new Phaser.Game(config);